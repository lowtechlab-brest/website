#!/usr/local/bin/bash
# -*coding: UTF-8 -*-


# Localisation du dossier synchronisé vers framagit:
export SITEWEB_ROOT="/Users/gmaze/git/github/gmaze/lowtech-lab-website"  # Modifier selon votre usage


# Active l'environment python:
# (see environment.yml)
source ~/miniconda3/etc/profile.d/conda.sh
conda activate lowtechlab


# Demarre la surveillance du dossier de destination des images du blog
# (fonction qui permet de generer des images basses resolutions)
#python "${SITEWEB_ROOT}/website/degradation_image.py" &
#image_monitor_pid=$!
#echo "Image upload monitoring process PID: $image_monitor_pid"


# Demarre la surveillance de la batterie du raspberry
# (avec remplissage automatique des donnees dans la basde SQLite3)
#python "${SITEWEB_ROOT}/flask/sys_raspberry.py" &


# Demarre le site web
flask --app "${SITEWEB_ROOT}/website" run --debug -p 5000
