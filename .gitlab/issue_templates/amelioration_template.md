## Proposition d'~Ameliorations


*Décrivez ici en quelques phrases votre proposition d'amélioration ou de modification du site web*

***

**Je pense que ma proposition:**
- [ ] est ~Possible (cad qui pourrait etre mise en oeuvre avec l'expertise du Low-Tech Lab Brest)
- [ ] ou si ~"De l'aide est requise" ou une montée en compétence interne nécessaire

**Je pense également que ma proposition:**
- [ ] est ~Indispensable à la réalisation d'un des [jalons de notre feuille de route: (les consulter ici)](https://framagit.org/lowtechlab-brest/website/-/milestones)
- [ ] ou ~Souhaitable, cad que ce serait *cool* mais pas une priorité

***