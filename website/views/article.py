from flask import (
    Blueprint,
    render_template,
    g,
    url_for,
    redirect,
    request,
    session,
    flash,
)
from website.utils import (
    fix_url_for_miniature,
    delete_miniatures,
    has_role,
)
import website.db as DB
from website.images import ImageManager


article = Blueprint('article', __name__)


@article.url_value_preprocessor
def get_post_id(endpoint, values):
    post_id = values.pop('post_id')
    g.post_id = post_id
    print("post_id:", post_id)


@article.route("/")
def display():
    post = DB.get_post_as_dict(g.post_id)

    if post:
        post = fix_url_for_miniature(post)

        is_loggedin_the_author = post['created_by'] == DB.get_user_by_email_as_dict(session.get("username", False))['id'] if session.get("username", False) else False

        can_modify = has_role('maintain') or is_loggedin_the_author
        return render_template("article.html",
                               post=post,
                               can_modify=can_modify,
                               can_suppr=can_modify,
                               active='blog')
    else:
        return render_template('error.html',
                               active='blog',
                               msg="L'article '%i' est introuvable" % g.post_id), 404


@article.route("/edit", methods=("GET", "POST"))
def edit():
    if has_role('write'):
        post = DB.get_post_as_dict(g.post_id)
        if request.method == "GET":
            return render_template("article_edit.html", post=post)
        elif request.method == "POST":
            title = request.form["title"]
            content = request.form["content"]
            miniature = request.files["miniature"]
            if not title:
                flash("Un titre est requis !")
            else:
                if miniature.filename:
                    f = ImageManager(miniature)  # Le fichier est telecharge automatiquement
                    filename = f.name_in_store  # pour l'enregistrement en base
                    delete_miniatures(post)  # Supprime l'ancienne image
                else:
                    filename = post['miniature']
                DB.update_post(title,
                               content,
                               g.post_id,
                               DB.get_user_by_email_as_dict(session.get("username"))['id'],
                               filename)
                return redirect(url_for(".display", post_id=g.post_id))
    else:
        return render_template("error.html", msg="Vous n'êtes pas autorisé.e à éditer cet article")


@article.route("/delete", methods=("GET", "POST"))
def delete():
    if session.get("id", False):
        post = DB.get_post_as_dict(g.post_id)
        if has_role('maintain') or session['id'] == post['created_by']:
            # Supprime les images du post:
            delete_miniatures(post)

            # Puis supprime le post de la base de donnees:
            DB.delete_post(g.post_id)

            flash('Article "{}" supprimé avec succès !'.format(post["title"]))
            return redirect(url_for("blog.display"))

        else:
            return render_template("error.html", msg="Vous n'êtes pas autorisé.e à supprimer cet article")
    else:
        return render_template("error.html", msg="Vous devez être connecté.e pour supprimer un article")


@article.route("/delete_image", methods=("GET", "POST"))
def delete_image():
    if session.get("id", False):
        post = DB.get_post_as_dict(g.post_id)
        if has_role('maintain') or session['id'] == post['created_by']:
            # Supprime uniquement les images du post:
            post = delete_miniatures(post)

            # Maj de la base:
            DB.update_post(post['title'],
                           post['content'],
                           g.post_id,
                           DB.get_user_by_email_as_dict(session.get("username"))['id'],
                           post['miniature'])

            flash('Images de "{}" supprimée avec succès !'.format(post["title"]))
        else:
            flash("Vous n'êtes pas autorisé.e à supprimer cette image")

        return redirect(url_for(".display", post_id=g.post_id))

    else:
        return render_template("error.html", msg="Vous devez être connecté.e pour supprimer cette image")
