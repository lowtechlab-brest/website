from flask import (
    Blueprint,
    render_template,
    request,
    session,
    jsonify,
)
from website.utils import has_role
import pandas as pd
import website.db as DB


admin = Blueprint('admin', __name__)


@admin.route("/users", methods=("GET", "POST"))
def users():
    if has_role('triage'):
        if request.method == "GET":

            users = DB.get_users_as_dict()
            data = pd.DataFrame(users)
            data['registration_date'] = data['registration_date'].astype('datetime64[ns]')
            data['last_connection'] = data['last_connection'].astype('datetime64[ns]')
            data['days_since_last_connection'] = pd.to_datetime('now', utc=False) - data['last_connection']

            # Remove hashed mdp, useless in displayed table:
            data = data.drop('mdp', axis=1)

            # Render table:
            # Define value and text of the dropdown:
            role_choices = (("read", "read"),
                            ("write", "write"),
                            ("maintain", "maintain"),
                            ("triage", "triage"),
                            ("admin", "admin"))
            col2htmltitle = {'id': 'ID',
                             'nom': 'Nom',
                             'prenom': 'Prénom',
                             'email': 'Email',
                             'role': 'Rôle',
                             'registration_date': 'Création',
                             'last_connection': 'Dernière connection',
                             'days_since_last_connection': 'Delta',
                             }
            column_names = [col2htmltitle[v] for v in data.columns.values]

            return render_template("admin_users.html",
                                   column_names=column_names,
                                   rows=data.to_dict('records'),
                                   link_column="role",
                                   choices=role_choices,
                                   )

        elif request.method == "POST":
            # Update user role
            userId = request.json['userId']
            new_role = request.json['new_role']
            this_user = DB.get_user_by_id_as_dict(userId)

            if new_role == 'admin' and session['role'] == 'triage':
                success = False
                msg = "Seuls les 'admin' peuvent accorder ce rôle à un nouvel utilisateur"
            elif new_role in ['read', 'write', 'maintain', 'triage', 'admin']:
                try:
                    DB.update_user_role(this_user, new_role)
                    success = DB.get_user_by_id_as_dict(userId)['role'] == new_role
                    msg = "Rôle mis à jour"
                except Exception as e:
                    success = False
                    msg = str(e)
            #
            return jsonify({
                "status": success,
                "msg": msg,
            })

    else:
        msg = "Vous n'avez pas les droits d'accés à cette page d'administration"
        return render_template("error.html", msg=msg)
