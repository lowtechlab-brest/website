from flask import (
    Blueprint,
    render_template,
    request,
    session,
    jsonify,
    abort,
    make_response,
)

import plotly
import plotly.express as px
import json
import dateutil
from dateutil import tz
from typing import Union
from website.utils import lr_state, datadict2csv
from website.monitors import read_save_data
import website.db as DB

api = Blueprint('api', __name__)


class InvalidAPIUsage(Exception):
    status_code = 400

    def __init__(self, message, status_code=None, payload=None):
        super().__init__()
        self.message = message
        if status_code is not None:
            self.status_code = status_code
        self.payload = payload

    def to_dict(self):
        rv = dict(self.payload or ())
        rv['message'] = self.message
        return rv


@api.errorhandler(InvalidAPIUsage)
def invalid_api_usage(e):
    return jsonify(e.to_dict()), e.status_code


@api.route("/img_res", methods=['GET'])
def image_quality():
    """Use this route to read or change the 'lr' session parameter controlling Image quality"""
    lr = request.args.get("lr", type=lambda x: x.lower() in ["1", "true"])

    if "set_lr" in request.args:
        # Apply new state to session:
        session["lr"] = lr

    return jsonify({"lr": lr_state()})


@api.route("/sensors", methods=['GET'])
def read_sensors():
    # /sensors
    # /sensors?n=1
    # /sensors?n=1000&q=pourcentage_BAT
    # /sensors?n=1000&q=pourcentage_BAT,luminosite
    # /sensors?n=1000&q=pourcentage_BAT,luminosite&format=csv

    # Lit et valide les arguments eventuels de la requete:

    n = request.args.get("n", default=DB.get_sensor_tablelen())
    n = valid_n_records(n)

    fields = request.args.get("q", default='tous')
    fields = valid_sensors_list(fields, errors='raise')
    fields = fields + ['record'] if 'record' not in fields else fields

    # Recupere les donnees:
    data = DB.get_sensors_data_as_dict(fields=fields, n=n)
    # print(data)

    # Possibly change data format:
    format = valid_format(request.args.get("format", default='json'))

    if format == 'json':
        return jsonify({"data": data})
    elif format == 'csv':
        data = datadict2csv(data)
        output = make_response(data)
        output.headers["Content-type"] = "text/plain; charset=utf-8"
        return output


@api.route("/records_length", methods=['GET'])
def read_n():
    n1 = DB.get_sensor_tablelen()
    n2 = DB.get_server_tablelen()
    return jsonify({"data": {'sensors': n1, 'server': n2}})


@api.route("/sensors_list", methods=['GET'])
def list_sensors():
    data = DB.get_sensor_list()
    data = DB.get_server_var_list()
    return jsonify({"data": data})


@api.route("/server_list", methods=['GET'])
def list_server():
    data = DB.get_server_var_list()
    return jsonify({"data": data})


@api.route("/server", methods=['GET'])
def read_server():
    # /server
    # /server?n=1
    # /server?n=1000&q=cpu
    # /server?n=1000&q=cpu,mem&format=csv

    # Lit et valide les arguments eventuels de la requete:

    n = request.args.get("n", default=DB.get_server_tablelen())
    n = valid_n_records(n)

    fields = request.args.get("q", default='tous')
    fields = valid_server_data_list(fields, errors='raise')
    fields = fields + ['record'] if 'record' not in fields else fields

    # Recupere les donnees:
    data = DB.get_server_data_as_dict(fields=fields, n=n)
    # print(data)

    # Possibly change data format:
    format = valid_format(request.args.get("format", default='json'))

    if format == 'json':
        return jsonify({"data": data})
    elif format == 'csv':
        data = datadict2csv(data)
        output = make_response(data)
        output.headers["Content-type"] = "text/plain; charset=utf-8"
        return output


@api.route("/gauges", methods=['GET'])
def read_gauges():
    """A gauge is a unique value out of a timeseries"""
    # /gauges
    # /gauges?q=cpu
    # /gauges?q=cpu,pourcentage_BAT

    #
    fields_server = request.args.get("q", default='tous')
    fields_server = valid_server_data_list(fields_server, errors='ignore')
    data_server = {}
    if len(fields_server):
        data_server = DB.get_server_data_as_dict(fields=fields_server, n=1)
        # data_server['record_server'] = data_server['record']
        # data_server.pop('record')


    fields_sensors = request.args.get("q", default='tous')
    fields_sensors = valid_sensors_list(fields_sensors, errors='ignore')
    data_sensors = {}
    if len(fields_sensors):
        data_sensors = DB.get_sensors_data_as_dict(fields=fields_sensors, n=1)
        # data_sensors['record_sensors'] = data_sensors['record']
        # data_sensors.pop('record')

    data = {**data_server, **data_sensors}
    if 'id' in data:
        data.pop('id')
    if 'record' in data:
        data.pop('record')

    return jsonify({"data": data})


@api.route("/graphJSON")
def graphe():
    """Return a JSON for a plotly timeseries"""
    sensor_data = DB.get_sensors_data_as_dict()
    # server_data = DB.get_server_data_as_dict()

    # Plotly doesn't currently support timezones:
    sensor_data['record'] = [dateutil.parser.isoparse(d) for d in sensor_data['record']]
    sensor_data['record'] = [d.astimezone(tz=dateutil.tz.tzlocal()) for d in sensor_data['record']]

    #
    fig = px.line(sensor_data, x='record', y='pourcentage_BAT', width=1000,
                  labels={
                     "record": "Temps",
                     "pourcentage_BAT": "Pourcentage de charge",
                     },
                title="Historique de la batterie")
    graphJSON = json.dumps(fig, cls=plotly.utils.PlotlyJSONEncoder)

    return jsonify({"data": graphJSON})


def valid_sensors_list(fields: Union[str, list], errors='raise') -> list:
    """Retourne une liste de variables valides ou une erreur"""
    fields_valid = DB.get_sensor_list()
    if fields == 'all' or fields == 'tous':
        fields = fields_valid
    else:
        fields = fields.split(',')
    # print("fields=", fields)

    # Valide la liste des variables demandees:
    not_valid = []
    validated = []
    for f in fields:
        if f not in fields_valid:
            not_valid.append(f)
        else:
            validated.append(f)
    if len(not_valid) > 0 and errors=='raise':
        raise InvalidAPIUsage("Nom(s) de variable invalide(s): [%s]" % (",".join(not_valid)),
                              payload={'request': request.args,
                                       'variables_autorisees':  [v for v in ['tous'] + fields_valid]})
    return validated


def valid_server_data_list(fields: Union[str, list], errors='raise') -> list:
    """Retourne une liste de variables valides ou une erreur"""
    fields_valid = DB.get_server_var_list()
    if fields == 'all' or fields == 'tous':
        fields = fields_valid
    else:
        fields = fields.split(',')
    # print("fields=", fields)

    # Valide la liste des variables demandees:
    not_valid = []
    validated = []
    for f in fields:
        if f not in fields_valid:
            not_valid.append(f)
        else:
            validated.append(f)
    if len(not_valid) > 0 and errors == 'raise':
        raise InvalidAPIUsage("Nom(s) de variable invalide(s): [%s]" % (",".join(not_valid)),
                              payload={'request': request.args,
                                       'variables_autorisees':  [v for v in ['tous'] + fields_valid]})
    return validated


def valid_gauge_data_list(fields: Union[str, list], errors='raise') -> list:
    """Retourne une liste de variables valides ou une erreur"""
    fields_server = DB.get_server_var_list()
    fields_sensors = DB.get_sensor_list()
    fields_valid = fields_server + fields_sensors
    fields_valid = list(dict.fromkeys(fields_valid))  # Remove possible duplicate from the list
    fields_valid.sort()

    if fields == 'all' or fields == 'tous':
        fields = fields_valid
    else:
        fields = fields.split(',')

    not_valid = []
    validated = []
    for key in fields:
        if key in fields_server or key in fields_sensors:
            validated.append(key)
        else:
            not_valid.append(key)

    if len(not_valid) > 0 and errors == 'raise':
        raise InvalidAPIUsage("Nom(s) de variable invalide(s): [%s]" % (",".join(not_valid)),
                              payload={'request': request.args,
                                       'variables_autorisees':  [v for v in ['tous'] + fields_valid]})
    else:
        validated = list(dict.fromkeys(validated))  # Remove duplicate from the list
        validated.sort()
        return validated


def valid_n_records(n) -> int:
    if str(n).isdigit() and int(n) > 0:
        return int(n)
    else:
        raise InvalidAPIUsage("Le nombre d'enregistrements demandes 'n' doit etre un entier strictement positif",
                              payload={'request': request.args})


def valid_format(f: str) -> str:
    fields_valid = ['json', 'csv']
    if f in fields_valid:
        return str(f)
    else:
        raise InvalidAPIUsage("Format invalide",
                              payload={'request': request.args, 'format_autorises':  [v for v in fields_valid]})

