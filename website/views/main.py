from flask import (
    Blueprint,
    render_template,
    request,
    session,
    jsonify,
)
from website.utils import lr_state


main = Blueprint('main', __name__)


@main.route("/")
def home():
    return render_template("index.html", active="home")


@main.route("/a_propos")
def a_propos():
    return render_template("a_propos.html", active="a_propos")
