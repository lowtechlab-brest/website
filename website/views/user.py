from flask import (
    Blueprint,
    render_template,
    url_for,
    redirect,
    request,
    session,
    flash,
    jsonify,
    current_app,
)
from werkzeug.security import check_password_hash
import re
import website.db as DB
from website.utils import has_role


user = Blueprint('user', __name__)


@user.route("/", methods=["GET"])
def account():
    if session.get("username", False):
        user_data = DB.get_user_by_email_as_dict(session.get("username"))
        return render_template("user.html", account=user_data)
    else:
        return redirect(url_for("user.register"))


@user.route("/register", methods=["GET", "POST"])
def register():
    if (
        request.method == "POST"
        and "email" in request.form
        and "nom" in request.form
        and "prenom" in request.form
        and "mdp" in request.form
    ):
        email = request.form["email"]
        nom = request.form["nom"]
        prenom = request.form["prenom"]
        mdp = request.form["mdp"]
        if DB.get_user_by_email_as_dict(email):
            flash("Cet email est déjà associé à un compte dans notre base. Utiliser un autre email.")
        elif not re.match(r"[^@]+@[^@]+\.[^@]+", email):
            flash("Email invalide")
        elif not nom or not prenom or not mdp or not email:
            flash("Remplir le formulaire !")
        else:
            # Si le compte n'existe pas:
            DB.insert_user(nom, prenom, email, mdp, role=current_app.config['DEFAULT_ROLE'])
            user_data = DB.get_user_by_email_as_dict(email)  # get new account data
            flash("Compte enregistré avec succés")

            # creer une session
            session["loggedin"] = True
            session["id"] = user_data["id"]
            session["name"] = "%s, %s" % (user_data["nom"], user_data["prenom"])
            session["username"] = user_data["email"]
            session["role"] = user_data["role"]

            return redirect(url_for("main.home"))
    elif request.method == "POST":
        # si le formulaire est vide
        flash("Remplir tous les champs !")

    # else return the registration page:
    return render_template("user_register.html")


@user.route("/login", methods=["GET", "POST"])
def login():
    if request.method == "POST" and "email" in request.form and "mdp" in request.form:
        email, mdp = request.form["email"], request.form["mdp"]

        # Cherche si le compte existe:
        user_data = DB.get_user_by_email_as_dict(email)
        if user_data:
            if check_password_hash(user_data["mdp"], mdp) == True:  # Mot de passe OK
                # Creation d'une session:
                session["loggedin"] = True
                session["id"] = user_data["id"]
                session["name"] = "%s %s" % (user_data["prenom"], user_data["nom"])
                session["username"] = user_data["email"]
                session["role"] = user_data["role"]
                DB.update_last_login(email)
                flash('Vous vous êtes connecté(e) avec succés')
                return redirect(url_for("main.home"))
            else:
                # Mot de passe incorrect:
                flash("Mot de passe INCORRECT")
        else:
            # Compte n'existe pas:
            flash("Cet email n'est pas dans notre base: '%s'" % email)
    return render_template("user_login.html")


@user.route("/logout")
def logout():
    # supprimer les données de sessions
    session.pop("loggedin", None)
    session.pop("id", None)
    session.pop("name", None)
    session.pop("username", None)
    session.pop("role", None)
    session.pop("lr", None)
    session.clear()
    return redirect(url_for("user.login"))


@user.route("/delete", methods=["POST"])
def delete():
    if session.get("username", False) and request.method == "POST":
        if 'email' in request.form:
            # Request coming from the user account page:
            email = request.form["email"]
            if has_role('admin') or email == session.get("username"):
                DB.delete_user(email)
                logout()
                flash('Compte "{}" supprimé avec succès !'.format(email))
                return redirect(url_for("main.home"))
            else:
                return render_template("error.html", msg="Impossible de supprimer ce compte ('%s')" % email)

        elif 'userId' in request.json:
            # Request coming from the admin users page:
            if has_role('admin'):
                userId = request.json['userId']
                try:
                    user_data = DB.get_user_by_id_as_dict(userId)
                    DB.delete_user(user_data['email'])
                    success = True
                    msg = "Compte id=%s (%s) supprimé avec succés !" % (userId, user_data['email'])
                except Exception as e:
                    success = False
                    msg = str(e)
                return jsonify({
                        "status": success,
                        "msg": msg,
                        "userId": userId,
                    })
            else:
                msg = "Vous n'avez pas assez de privilèges pour supprimer le compte d'un utilisateur"
                return render_template("error.html", msg=msg)



@user.route("/edit", methods=["GET", "POST"])
def edit():
    if session.get("username", False) and request.method == "POST":
        previous_email = session['username']
        if (
            "email" in request.form
            and "nom" in request.form
            and "prenom" in request.form
        ):
            email = request.form["email"]
            nom = request.form["nom"]
            prenom = request.form["prenom"]
            user_data = DB.get_user_by_email_as_dict(email)
            if user_data:
                if not re.match(r"[^@]+@[^@]+\.[^@]+", email):
                    flash("Email invalide")
                elif email != previous_email:
                    # Check if new email is not used by another user:
                    if DB.get_user_by_email_as_dict(email) is not None:
                        flash("Cet email est déjà utilisé par un(e) autre utilisateur(trice)")
                        user_data = DB.get_user_by_email_as_dict(previous_email)
                        pass

                else:
                    DB.update_user(previous_email, nom, prenom, email)
                    # Update session with new data:
                    user_data = DB.get_user_by_email_as_dict(email)
                    session["id"] = user_data["id"]
                    session["name"] = "%s, %s" % (user_data["nom"], user_data["prenom"])
                    session["username"] = user_data["email"]
                    session["role"] = user_data["role"]
                    session["email"] = user_data["email"]
                    flash("Informations mises à jour")

    return render_template("user.html", account=user_data)
