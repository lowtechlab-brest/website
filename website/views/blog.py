from flask import (
    Blueprint,
    render_template,
    url_for,
    redirect,
    request,
    session,
    flash,
)
from website.utils import (
    fix_url_for_miniature,
    has_role,
)
import website.db as DB
from website.images import ImageManager


blog = Blueprint('blog', __name__)


@blog.route("/")
def display():
    posts = DB.get_posts_as_dict()
    posts_to_publish = []
    if len(posts) > 0:
        for p in posts:
            p = fix_url_for_miniature(p)
            posts_to_publish.append(p)

        # Trie les articles pour que le plus recent soit en premier:
        posts_to_publish.sort(key=lambda k: k.get("created"), reverse=True)

    #
    return render_template(
        "blog.html", posts=posts_to_publish, can_write=has_role('write'), active="blog"
    )


@blog.route("/create", methods=("GET", "POST"))
def create():
    if has_role('write'):
        if request.method == "POST":
            account = DB.get_user_by_email_as_dict(session.get("username"))
            title = request.form["title"]
            content = request.form["content"]
            miniature = request.files["miniature"]
            # annexe = request.files['annexe']
            if not title:
                flash("Un titre est requis !")
            else:
                if miniature.filename:
                    f = ImageManager(miniature)  # Le fichier est telecharge automatiquement
                    filename = f.name_in_store
                else:
                    filename = ""
                DB.insert_post(title, content, filename, account['id'])
                return redirect(url_for(".display"))
        return render_template("article_create.html")
    else:
        flash("Vous n'êtes pas autorisé à écrire des articles")
        return redirect(url_for("profile.login"))
