from flask import (
    Blueprint,
    render_template,
)
from datetime import datetime
import pandas as pd
import plotly
import plotly.express as px
import json
import dateutil
from dateutil import tz
from website.utils import (
    plot_save_battery,
    plot_save_courant,
    plot_save_cpu,
)
import website.db as DB


dashboard = Blueprint('dashboard', __name__)


# @dashboard.route("/")
def deprecated():  # Not interactive plots
    sensor_data = DB.get_sensors_data_as_dict()
    server_data = DB.get_server_data_as_dict()

    # Nb of points to display
    Record_len = len(sensor_data['record']) if len(sensor_data['record']) < 1e6 else 1e6

    plot_save_battery(sensor_data, Record_len)
    plot_save_courant(sensor_data, Record_len)
    # plot_save_cpu(server_data, Record_len)
    return render_template("dashboard.html",
                           mem=server_data['mem'][0],
                           cpuload=server_data['cpu'][0],
                           batterie=sensor_data['pourcentage_BAT'][0],
                           active="dashboard")


# @dashboard.route("/")
def another_deprecated():  # Too long to load
    sensor_data = DB.get_sensors_data_as_dict()
    # server_data = DB.get_server_data_as_dict()

    # Plotly doesn't currently support timezones:
    sensor_data['record'] = [dateutil.parser.isoparse(d) for d in sensor_data['record']]
    sensor_data['record'] = [d.astimezone(tz=dateutil.tz.tzlocal()) for d in sensor_data['record']]

    #
    print(datetime.now())
    fig = px.line(sensor_data, x='record', y='pourcentage_BAT', width=1000,
                  labels={
                     "record": "Temps",
                     "pourcentage_BAT": "Pourcentage de charge",
                     },
                title="Historique de la batterie")
    fig.update_yaxes(range=[0, 105])
    print(datetime.now())

    graphJSON = json.dumps(fig, cls=plotly.utils.PlotlyJSONEncoder)
    print(datetime.now())

    return render_template("dashboard.html",
                           graphJSON_batt=graphJSON,
                           active="dashboard")


@dashboard.route("/")
def display():
    """ Load dashbaord

    All data for graphs are loaded from the web page using the api route
    """
    return render_template("dashboard.html",
                           active="dashboard")
