# -*coding: UTF-8 -*-
#
# HELP
#
# Created by gmaze on 23/10/2023

import os

from flask import session, url_for, request
from flask import current_app as app
import functools

from babel import Locale
locale = Locale('fr', 'FR')
from babel.dates import format_datetime as babel_format_datetime
from babel.dates import format_timedelta as babel_format_timedelta

import matplotlib as mpl
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
import dateutil.parser

from website.images import ImageManager
import website.db as DB


mpl.use("Agg")

APP_NAME = __name__.split(".")[0]


def lr_state():
    """Return image resolution 'lr' from request, session or fall back on config"""
    if "lr" in request.args:
        lr = request.args.get("lr", type=lambda x: x.lower() in ["1", "true"])
        # print('using lr from request:', lr)
    elif "lr" in session:
        lr = session["lr"]
        # print('using lr from session:', lr)
    else:
        with app.app_context():
            lr = app.config["LR"]
        # print('using lr from app config:', lr)

    # Update session with value found:
    session["lr"] = lr
    # print("session['lr']=", session['lr'])
    return lr


def fix_url_for_miniature(a_post: dict) -> dict:
    if a_post['miniature'] is not None and a_post['miniature'] != "":
        lr = lr_state()
        try:
            img = ImageManager(a_post["miniature"])
        except:
            # print(a_post)
            raise
        if lr:
            # Use Low-res image:
            file_to_use = img.lowres
        else:
            # Use High-res image (original file is assumed to be high res.):
            file_to_use = img.original

        a_post["miniature"] = url_for("static", filename=file_to_use)
    return a_post


def delete_miniatures(p):
    if p['miniature'] is not None and p['miniature'] != "":
        img = ImageManager(p["miniature"])
        img.delete()
        p['miniature'] = ""
    return p


@functools.total_ordering
class UserRole:
    """
    Helper class for user's role

    Examples
    --------
    r = role('write')
    r > role('triage')  # False
    r.has_role('read')  # True

    """
    rolec = {'read': 0,
               'write': 100,
               'maintain': 200,
               'triage': 300,
               'admin': 400}

    def __init__(self, value):
        self.valid = self.rolec.keys()
        if value in self.valid:
            self.value = value
        else:
            raise ValueError("Invalid role, must be one in: %s" % (",".join(self.valid)))

    def __repr__(self):
        t = ["<UserRole>"]
        t.append("Name: %s" % self.value)
        t.append("Value: %i" % self.id)
        return "\n".join(t)

    @property
    def id(self) -> int:
        return self.rolec[self.value]

    def __eq__(self, other):
        if not isinstance(other, UserRole):
            raise TypeError()
        else:
            return self.id == other.id

    def __ge__(self, other):
        if not isinstance(other, UserRole):
            raise TypeError()
        else:
            return self.id >= other.id

    def has_role(self, str):
        if str not in self.valid:
            raise ValueError("Invalid role, must be one in: %s" % (",".join(self.valid)))
        else:
            return self >= UserRole(str)


def has_role(requested_role: str):
    """Return true if app session role is appropriate otherwise false

    Implement accumulation of privileges:

    'read' < 'write' < 'maintain' < 'triage' < 'admin'

    Examples
    --------
    if has_role('write'):
        DB.update_post(...

    """
    # Get session's role :
    if 'role' in session:
        return UserRole(session['role']).has_role(requested_role)
    else:
        return False


def format_datetime(value, format='medium'):
    if format == 'full':
        format="d MMMM y 'à' HH:mm"
    elif format == 'medium':
        format="d MMMM y"
    elif format == 'short':
        format = "dd/MM/y"
    return babel_format_datetime(value, format, locale=locale)


def format_timedelta(value, format='day'):
    return babel_format_timedelta(value, granularity=format, locale=locale)


def plot_a_timeseries(x, y, label='?'):

    with mpl.rc_context({'timezone': 'Europe/Paris'}):
        fig, ax = plt.subplots(figsize=(14, 6), dpi=90)
        ax.plot(x, y, 'r')
        ax.set_xlabel("Temps")
        ax.set_ylabel("")
        ax.set_title(label)
        ax.grid(True)

        ax.xaxis.set_minor_locator(mdates.AutoDateLocator())
        ax.xaxis.set_major_locator(mdates.AutoDateLocator())
        ax.xaxis.set_major_formatter(mdates.DateFormatter('%d/%m %H:%M:%S'))
        fig.autofmt_xdate()

    return fig, ax

def plot_save_battery(data, Record_len):
    pourcent_bat, date = data['pourcentage_BAT'], data['record']
    pourcent_bat = pourcent_bat[0:Record_len]
    date = date[0:Record_len]

    if len(pourcent_bat):
        date = [dateutil.parser.isoparse(d) for d in date]

        fig, ax = plot_a_timeseries(date, pourcent_bat, label="Historique du Pourcentage de charge de la batterie")

        fichier = os.path.abspath(
            os.path.sep.join(
                [".", APP_NAME, "static", "ressource", "graph", "graph_bat.png"]
            )
        )
        fig.savefig(fichier, bbox_inches='tight')
    else:
        fichier = None

    return fichier


def plot_save_courant(data, Record_len):
    courant, date = data['courant'], data['record']
    courant = courant[0:Record_len]
    date = date[0:Record_len]

    if len(courant):
        date = [dateutil.parser.isoparse(d) for d in date]

        fig, ax = plot_a_timeseries(date, courant, label="Historique du Courant dans la batterie")

        fichier = os.path.abspath(
            os.path.sep.join(
                [".", APP_NAME, "static", "ressource", "graph", "graph_courant.png"]
            )
        )
        fig.savefig(fichier, bbox_inches='tight')
    else:
        fichier = None
    return fichier


def plot_save_cpu(data, Record_len):
    cpu, date = data['cpu'], data['record']
    cpu = cpu[0:Record_len]
    date = date[0:Record_len]

    if len(cpu):
        date = [dateutil.parser.isoparse(d) for d in date]

        fig, ax = plot_a_timeseries(date, cpu, label="Historique de charge du CPU du serveur")

        fichier = os.path.abspath(
            os.path.sep.join(
                [".", APP_NAME, "static", "ressource", "graph", "graph_cpu.png"]
            )
        )
        fig.savefig(fichier, bbox_inches='tight')
    else:
        fichier = None
    return fichier


def read_monitoring_data():
    try:
        # Read data from db:
        cpu, mem = DB.get_monitoring_data_for_graphs()

        # Process data:
        cpu1 = list(cpu[-1])
        mem1 = list(mem[-1])
    except:
        mem1, cpu1 = [], []

    return mem1, cpu1


def datadict2csv(data):
    """Conversion d'un dictionnaire de series de donnees au format csv"""

    # Les cles seront les colonnes
    cols = [k for k in data.keys()]

    #
    rows = [",".join(cols)]
    for iter, row in enumerate(data[cols[0]]):  # On fait l'hypothese que toutes les series ont la meme longueur !
        this_row = [str(data[col][iter]) for col in cols]
        rows.append(",".join(this_row))

    data = "\n".join(rows)

    return data