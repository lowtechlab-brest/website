from flask import Flask, render_template
import secrets
import os
from logging.config import dictConfig
from . import db as DB
from .utils import (
    has_role,
    format_datetime,
    format_timedelta,
)
from .monitors import Monitor


dictConfig({
    'version': 1,
    'formatters': {'default': {
        'format': '[%(asctime)s] %(levelname)s in %(module)s: %(message)s',
    }},
    'handlers': {'wsgi': {
        'class': 'logging.StreamHandler',
        'stream': 'ext://flask.logging.wsgi_errors_stream',
        'formatter': 'default'
    }},
    'root': {
        'level': 'INFO',
        'handlers': ['wsgi']
    }
})

app = Flask(__name__)
# app.config['SESSION_TYPE'] = 'filesystem'
app.config['SECRET_KEY'] = 'dev'
# app.config['SECRET_KEY'] = '7z3V98deMEiYRfF2x4i9'
# app.config['SECRET_KEY'] = secrets.token_hex()

# app.config.from_pyfile('../config_flask.py', silent=True)
app.config.from_object('config_flask')

APP_ROOT = os.path.dirname(os.path.abspath(__file__))
app.config['MINIATURE'] = os.path.join('photos', 'miniature')
UPLOAD_FOLDER = os.path.join(APP_ROOT, 'static', app.config['MINIATURE'])
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER

# Add some useful variables to the jinja context:
with app.app_context():
    # Add the battery level indicator:
    # pourcent_battery_recupere is used in "partials/_footer.html" partials template
    app.jinja_env.globals.update(pourcent_battery_recupere=DB.get_battery_level())
    app.jinja_env.globals.update(has_role=has_role)
    # Add custom filters:
    app.jinja_env.filters["format_datetime"] = format_datetime
    app.jinja_env.filters["format_timedelta"] = format_timedelta


# Import all routes from blueprints:
from .views.main import main
app.register_blueprint(main)

from .views.user import user
app.register_blueprint(user, url_prefix='/user')

from .views.blog import blog
app.register_blueprint(blog, url_prefix='/blog')

from .views.article import article
app.register_blueprint(article, url_prefix='/blog/<int:post_id>')

from .views.admin import admin
app.register_blueprint(admin, url_prefix='/admin')

from .views.dashboard import dashboard
app.register_blueprint(dashboard, url_prefix='/dashboard')

from .views.api import api
app.register_blueprint(api, url_prefix='/api')


# Gestionnaire par default des erreurs 404:
@app.errorhandler(404)
def page_not_found(e):
    return render_template('error.html', msg="404: ressource introuvable"), 404


# Demarrage du monitoring du serveur:
# (creation d'un thread qui sera terminé automatiquement en meme temps que l'appli Flask)
Monitor(app_context=app.app_context(), refresh_rate=app.config['VALEURS_CAPTEURS_REFRESH']).start()


if __name__ == '__main__':
    print(app.instance_path)
    print("app.config['DATABASE'] = ", app.config['DATABASE'])
    app.run()
