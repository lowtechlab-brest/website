draw_a_gauge = function(gauge_name, opts, html_id, resfresh_rate, api){

    if (opts=='red'){
        var steps = [
                      { range: [90, 100], color: "red" },
                      { range: [75, 90], color: "orange" }
                    ]
    } else if (opts =='green'){
        var steps = [
                      { range: [90, 100], color: "green" },
                      { range: [75, 90], color: "lime" }
                    ]
    }

    let params = {'q': gauge_name[0]}
    let url = api + "?" + new URLSearchParams(params);

    draw_this();

    function draw_this(){
//         console.log("Resfrehing data from: " + url)
        d3.json(url, function(data) {
            var data = [
                {
                    domain: {x: [0, 1], y: [0, 1]},
                    value: data['data'][gauge_name[0]][0],
                    title: {text: gauge_name[1], font: { size: 18 }},
                    type: "indicator",
                    mode: "gauge+number",
                    //mode: "gauge+number+delta",
                    //delta: { reference: 50 },
                    gauge: {
                        bar: {color: 'lightgray'},
                        shape: 'angular',
                        axis: {range: [0, 100], dtick:20, tickwidth: 1, tickcolor: "darkblue" },
                        steps: steps,
                    }
                }
            ];

            var layout = {width: 350, height: 200, margin: {t: 0, b: 0}};
            Plotly.react(html_id, data, layout);
        });

        // draw again in 5 seconds
        window.setTimeout(draw_this, resfresh_rate*1000);
   }

};

draw_a_timeseries = function(tsname, yrange, html_id, resfresh_rate, api){
    let params = {'q': tsname[0], 'format': 'csv'}
//     console.log(tsname[0]);
//     if (tsname[0] == 'cpu'){
//         let params = {'q': tsname[0], 'format': 'csv', 'n': 60}
//     }
    let url = api + "?" + new URLSearchParams(params);

    draw_this();

    function draw_this(){
//         console.log("Resfrehing data from: " + url)
        d3.csv(url, function(err, rows) {
            function unpack(rows, key) {
                return rows.map(function(row) {
                    return row[key];
                });
            }

            var trace1 = {
                type: "scatter",
                mode: "lines",
                name: tsname[0],
                x: unpack(rows, 'record'),
                y: unpack(rows, tsname[0]),
                line: {
                    color: '#17BECF'
                }
            }
            if (tsname[0] == 'cpu'){
                trace1['x'] = trace1['x'].slice(1, 5*60);
                trace1['y'] = trace1['y'].slice(1, 5*60);
            }

            var data = [trace1];

            var layout0 = {
                title: tsname[1],
                yaxis: {
                    autorange: false,
                    range: yrange,
                    type: 'linear'
                },
                xaxis: {
                    type: 'date'
                }
            };

            Plotly.react(html_id, data, layout0);
        });

        // draw again in n seconds:
        window.setTimeout(draw_this, resfresh_rate*1000);
    }


}