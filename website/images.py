import os
from PIL import Image
import logging
from flask import current_app as app
from werkzeug.security import safe_join
from werkzeug.datastructures import FileStorage
import hashlib


try:
    UPLOAD_FOLDER = app.config["UPLOAD_FOLDER"]
except:
    APP_ROOT = os.path.dirname(os.path.abspath(__file__))  # Absolute path to Flask website
    UPLOAD_FOLDER = os.path.join(APP_ROOT, 'static', 'photos', 'miniature')

# Logger
log = logging.getLogger("lowtechlab.website.images")  # Logger


class ImageManager:
    """Class to handle image files

    - Create an instance with the name of a file to upload
    - Get file name in original/high and low resolutions as properties
    - Check if file name is already original or lowres

    Examples
    --------
    # Used with a html form result:
    f = ImageManager(request.files["miniature"])  # File will be uploaded automatically

    # A File name as recored in db, already in the upload folder:
    f = ImageManager('d03e1303e5b6780ff431248dcfd05db39ae3ba7158ec48aded598645ca4ccb5e')

    # Signature:
    f.name_in_store
    f.original  # Return static path to original file
    f.lowres    # Return static path to low-resolution file
    f.delete()  # Delete all related files
    """

    def _fichier2filenames(self, fichier):
        """Given a file name return all necessary file data"""
        data = {}
        file_name, file_extension = os.path.splitext(fichier)
        file_name = os.path.basename(file_name)

        # Avant d'aller plus loin, on verifie si on a besoin de hasher le fichier
        possibly = safe_join(app.config["UPLOAD_FOLDER"], file_name) + "_original"
        if os.path.exists(possibly):
            file_hash = file_name
            local_file = possibly
            log.debug("Ce fichier existe deja: %s" % local_file)
        else:
            file_hash = hashlib.sha256(file_name.encode()).hexdigest()
            local_file = safe_join(app.config["UPLOAD_FOLDER"], file_hash) + "_original"
            log.debug("Ce fichier n'existe pas: %s" % local_file)

        data['name'] = file_name
        data['hash'] = file_hash
        data['ext'] = file_extension
        data['local'] = local_file
        data['local_lowres'] = local_file.replace("_original", "_lowres")
        return data

    def __init__(self, obj):
        # Extract data de l'objet:
        self.fichier = self._fichier2filenames(obj.filename if isinstance(obj, FileStorage) else obj)

        # Avant d'aller plus loin, on verifie si cette image est deja sur le serveur ou pas:
        is_here = os.path.exists(self.fichier['local'])

        if not is_here:
            # et on telecharge le fichier sur le serveur si c'est necessaire:
            if isinstance(obj, FileStorage):
                obj.save(self.fichier['local'])

        # On verifie ensuite si la version basse resolution existe deja
        # et on la cree si necessaire:
        if not os.path.exists(self.fichier['local_lowres']):
            self.save2lowres()

        # L'initialisation est terminee
        # print(self.fichier)

    @property
    def name_in_store(self):
        return self.fichier['hash']

    @property
    def original(self):
        """Return relative path to original file"""
        return self.fichier['local'].replace(UPLOAD_FOLDER, app.config['MINIATURE'])

    @property
    def lowres(self):
        """Return relative path to low-resolution file"""
        return self.fichier['local_lowres'].replace(UPLOAD_FOLDER, app.config['MINIATURE'])

    def save2lowres(self):
        # Charge l'image d'origine:
        img = Image.open(self.fichier['local'])

        # Applique un filtre de dithering:
        img = img.convert(mode="1", dither=Image.FLOYDSTEINBERG)

        # Enregistre l'image transformée avec une qualité de 50 et une compression optimisée
        img.save(
            self.fichier['local_lowres'],
            optimize=True,
            quality=1,
            format='jpeg',
        )

        return self

    def delete(self):
        os.remove(self.fichier['local'])
        os.remove(self.fichier['local_lowres'])
