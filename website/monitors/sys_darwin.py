#!/usr/bin/env python
# -*coding: UTF-8 -*-
#
# HELP
#
# Created by gmaze on 12/11/2023
__author__ = 'gmaze@ifremer.fr'

import psutil
import website.db as DB
from datetime import datetime, timezone


def get_battery_level():
    try:
        return psutil.sensors_battery().percent
    except:
        return None


def get_cpu_level():
    try:
        return psutil.cpu_percent()
    except:
        return None


def read_save_data(save=True, app_context=None):
    date_jour = datetime.now(timezone.utc)

    # Capteurs:
    Tension = 0
    Courant = 0
    Pourcentage_BAT = get_battery_level()
    Luminosite = 0

    # Metrics serveur:
    cpu = get_cpu_level()
    mem = 0

    #
    if save:
        with app_context:
            DB.insert_sensor(date_jour, Tension, Courant, Pourcentage_BAT, Luminosite)
            DB.insert_server(date_jour, cpu, mem)

    return (date_jour, Tension, Courant, Pourcentage_BAT, Luminosite, cpu, mem)
