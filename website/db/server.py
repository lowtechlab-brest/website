from .utils import db_ops
from flask import current_app
from datetime import datetime, timezone, timedelta



def get_monitoring_data_for_graphs():
    """Custom data retrieval for the 'graphique' route"""
    with db_ops('server') as cursor:
        cpu = cursor.execute("SELECT cpu from server;").fetchall()
        mem = cursor.execute("SELECT mem from server;").fetchall()
    return cpu, mem


def get_server_tablelen():
    sql = "SELECT COUNT(*) FROM server;"
    with db_ops('server') as cursor:
        row = cursor.execute(sql).fetchone()
    return row[0]


def get_server_var_list():
    with db_ops('server') as cursor:
        c = cursor.execute("SELECT * FROM server LIMIT 1")
        keys = list(map(lambda x: x[0], c.description))
    return keys


def get_oldest_server_data():
    with db_ops('server') as cursor:
        row = cursor.execute(
            "SELECT * FROM server ORDER BY record ASC LIMIT 1"
        ).fetchone()
    return row


def get_oldest_server_data_as_dict() -> dict:
    # Get table columns:
    keys = get_server_var_list()
    # Get data:
    data = get_oldest_server_data()
    # Make a dictionary:
    result = {}
    if data:
        for n, p in zip(keys, data):
            result[n] = p
        return result
    else:
        return None


def insert_server(created, cpu, mem):
    """Ajoute une serie de mesures

    On verifie a chaque fois que le tableau ne devient pas trop gros
    """

    # Au dela de n enregistrements, on supprime le dernier pour laisser la place au nouveau::
    if get_server_tablelen() >= current_app.config['VALEURS_CAPTEURS_MAXLEN']:
        oldest_record = get_oldest_server_data_as_dict()
        with db_ops('server') as cursor:
            cursor.execute("DELETE FROM server WHERE id = ?", (oldest_record['id'],))

    # On supprime aussi les mesures trops vieilles:
    with db_ops('server') as cursor:
        age = datetime.now(timezone.utc) - timedelta(seconds=current_app.config['VALEURS_CAPTEURS_MAXAGE'])
        cursor.execute("DELETE FROM server WHERE record < ?", (age,))

    # Ajoute le nouvel enregistrement:
    sql = "INSERT INTO server (record, cpu, mem) VALUES (?,?,?)"
    with db_ops('server') as cursor:
        cursor.execute(
            sql, (created, cpu, mem)
        )


def get_server_data(n=None):
    if n is None:
        args = {"SELECT * FROM server ORDER BY record DESC"}
    else:
        args = ("SELECT * FROM server ORDER BY record DESC LIMIT ?", (str(n),))
    with db_ops('server') as cursor:
        row = cursor.execute(*args).fetchall()
    return row


def get_server_data_as_dict(fields=None, n=None) -> dict:
    # Argument 'fields' par default, ('n' default sera dans  la fonction 'get_server_data'):
    if fields is None:
        # Get table columns:
        fields = get_server_var_list()

    # Get data:
    data = get_server_data(n=n)

    # Make a dictionary of lists:
    result = {}
    if data:
        for k in fields:
            result[k] = []
        for row in data:
            for k in fields:
                result[k].append(row[k])
        return result
    else:
        return None

