import sqlite3
from flask import current_app
from contextlib import contextmanager


@contextmanager
def db_ops(db_name: str):
    conn = sqlite3.connect(current_app.config["DATABASE"][db_name], check_same_thread=False,)
    conn.row_factory = sqlite3.Row
    try:
        cur = conn.cursor()
        yield cur
    except Exception as e:
        conn.rollback()
        raise e
    else:
        conn.commit()
    finally:
        conn.close()
