#
# Ici on ne verifie pas si l'utilisateur a les droits
# La verif des droits est de la responsabilite de views.py
#
from werkzeug.exceptions import abort
from .utils import db_ops
from .user import get_user_by_id_as_dict
from datetime import datetime, timezone
from dataclasses import dataclass


@dataclass(init=False)
class Post:
    def __init__(self,
                 id: int,
                created: str,
                title: str,
                content: str,
                miniature: str,
                created_by: int,
                modified_by: str,
                modified: str,
    ):
        self.id = id
        self.created = datetime.fromisoformat(created)
        self.created_by = created_by
        self.title = title
        self.content = content
        self.miniature = miniature
        self.modified = datetime.fromisoformat(modified)
        self.modified_by = modified_by

    def to_dict(self):
        result = {}
        for k in ['id',
                  'created', 'created_by',
                  'title', 'content', 'miniature',
                  'modified', 'modified_by'
                  ]:
            result[k] = getattr(self, k)

        # Add information from the creator:
        result['author'] = get_user_by_id_as_dict(result['created_by'])

        # Add information from the author of the last modification:
        result['modifier'] = get_user_by_id_as_dict(result['modified_by'])

        return result


def get_post(post_id):
    with db_ops('post') as cursor:
        cursor.row_factory = lambda cursor, row: Post(*row)
        post: Post = cursor.execute("SELECT * FROM posts WHERE id = ?", (post_id,)).fetchone()
    return post


def get_post_as_dict(post_id):
    post = get_post(post_id)
    if post:
        return post.to_dict()
    else:
        return post


def get_posts():
    with db_ops('post') as cursor:
        cursor.row_factory = lambda cursor, row: Post(*row)
        posts: list[Post] = cursor.execute("SELECT * FROM posts").fetchall()
    return posts


def get_posts_as_dict():
    posts = get_posts()
    if len(posts) > 0:
        return [p.to_dict() for p in posts]
    else:
        return posts


def insert_post(title, content, img_file, author_id):
    sql = "INSERT INTO posts (title, content, miniature, created_by, modified_by, modified) VALUES (?,?,?,?,?,?)"
    with db_ops('post') as cursor:
        cursor.execute(
            sql, (title, content, img_file, author_id, author_id, datetime.now(timezone.utc))
        )


def update_post(title, content, post_id, author_id, miniature):
    sql = "UPDATE posts SET title = ?, content = ?, modified_by = ?, modified = ?, miniature = ? WHERE ID = ?"
    with db_ops('post') as cursor:
        cursor.execute(
            sql, (title, content, author_id, datetime.now(timezone.utc), miniature, post_id)
        )


def delete_post(id):
    with db_ops('post') as cursor:
        cursor.execute("DELETE FROM posts WHERE id = ?", (id,))
