from werkzeug.security import generate_password_hash
from datetime import datetime
from .utils import db_ops


def get_users():
    with db_ops('user') as cursor:
        users = cursor.execute("SELECT * FROM users").fetchall()
    return users


def get_user_by_id(id: int):
    with db_ops('user') as cursor:
        account = cursor.execute(
            "SELECT * FROM users WHERE id = ? ", (id,)
        ).fetchone()
    return account


def get_user_by_email(email):
    with db_ops('user') as cursor:
        account = cursor.execute(
            "SELECT * FROM users WHERE email = ? ", (email,)
        ).fetchone()
    return account


def get_user_by_email_as_dict(email: str) -> dict:
    # Get table columns:
    with db_ops('user') as cursor:
        c = cursor.execute("SELECT * FROM users")
        names = list(map(lambda x: x[0], c.description))
    # Get user data:
    account = get_user_by_email(email)
    # Make a dictionary:
    user = {}
    if account:
        for n, p in zip(names, account):
            user[n] = p
    return user if "id" in user else None


def get_user_by_id_as_dict(id: int) -> dict:
    # Get table columns:
    with db_ops('user') as cursor:
        c = cursor.execute("SELECT * FROM users")
        names = list(map(lambda x: x[0], c.description))
    # Get user data:
    account = get_user_by_id(id)
    # Make a dictionary:
    user = {}
    if account:
        for n, p in zip(names, account):
            user[n] = p
    return user if "id" in user else None


def get_user_by_name(name):
    with db_ops('user') as cursor:
        account = cursor.execute(
            "SELECT * FROM users WHERE nom = ? ", (name,)
        ).fetchone()
    return account


def get_users_as_dict():
    # Get table columns:
    with db_ops('user') as cursor:
        c = cursor.execute("SELECT * FROM users")
        names = list(map(lambda x: x[0], c.description))
    # Get posts data:
    posts = get_users()
    # Make a dictionary:
    results = []
    if posts:
        for post in posts:
            result = {}
            for n, p in zip(names, post):
                result[n] = p
            results.append(result)
    return results


def delete_user(email: str):
    with db_ops('user') as cursor:
        cursor.execute("DELETE FROM users WHERE email = ?", (email,))


def insert_user(nom, prenom, email, mdp, role):
    hmdp = generate_password_hash(mdp, method="pbkdf2:sha1", salt_length=8)
    sql = "INSERT INTO users VALUES (NULL, ?, ?, ?, ?, ?, ?, ?)"
    with db_ops('user') as cursor:
        cursor.execute(
            sql, (nom, prenom, email, hmdp, role, datetime.now(), datetime.now(), )
        )


def update_user(previous_email, nom, prenom, email):
    sql = "UPDATE users SET nom = ?, prenom = ?, email = ?" " WHERE email = ?"
    with db_ops('user') as cursor:
        cursor.execute(
            sql, (nom, prenom, email, previous_email),
        )

def update_user_role(a_user: dict, new_role: str):
    """Assume arguments are valid"""
    sql = "UPDATE users SET role = ? WHERE email = ?"
    with db_ops('user') as cursor:
        cursor.execute(
            sql, (new_role, a_user['email'])
        )


def update_last_login(email):
    sql = "UPDATE users SET last_connection = ? WHERE email = ?"
    with db_ops('user') as cursor:
        cursor.execute(
            sql, (datetime.now(), email, ),
        )
