from .utils import db_ops
from flask import current_app
from datetime import datetime, timezone, timedelta


def get_battery_level() -> float:
    """Return latest battery (percentage) level

    'latest' means the last value available from the database
    """
    # requête SQL d'acquisition des données de batterie en fonction de la date pour obtenir le plus récent:
    with db_ops('sensor') as cursor:
        row = cursor.execute(
            "SELECT pourcentage_BAT FROM VALEURS_CAPTEURS ORDER BY record DESC LIMIT 1"
        ).fetchone()

    if row is not None:
        pourcent = row[0]  # row[0] stock désormais la valeur prise dans la bdd
    else:
        pourcent = 0.0

    return pourcent


def get_sensor_data_for_graphs(n=None):
    """Custom data retrieval for the battery dashboard page"""
    data = get_sensors_data_as_dict(fields=['record', 'pourcentage_BAT', 'courant'], n=n)
    return data['pourcentage_BAT'], data['record'], data['courant']


def get_sensor_tablelen():
    sql = "SELECT COUNT(*) FROM VALEURS_CAPTEURS;"
    with db_ops('sensor') as cursor:
        row = cursor.execute(sql).fetchone()
    return row[0]


def get_sensor_list():
    with db_ops('sensor') as cursor:
        c = cursor.execute("SELECT * FROM VALEURS_CAPTEURS LIMIT 1")
        keys = list(map(lambda x: x[0], c.description))
    return keys


def insert_sensor(created, Tension, Courant, Pourcentage_BAT, Luminosite):
    """Ajoute une serie de mesures

    On verifie a chaque fois que le tableau ne devient pas trop gros
    """

    # Au dela de n enregistrements, on supprime le dernier pour laisser la place au nouveau::
    if get_sensor_tablelen() >= current_app.config['VALEURS_CAPTEURS_MAXLEN']:
        oldest_record = get_oldest_sensors_data_as_dict()
        with db_ops('sensor') as cursor:
            cursor.execute("DELETE FROM VALEURS_CAPTEURS WHERE id = ?", (oldest_record['id'],))

    # On supprime aussi les mesures trops vieilles:
    with db_ops('sensor') as cursor:
        age = datetime.now(timezone.utc) - timedelta(seconds=current_app.config['VALEURS_CAPTEURS_MAXAGE'])
        cursor.execute("DELETE FROM VALEURS_CAPTEURS WHERE record < ?", (age,))

    # Ajoute le nouvel enregistrement:
    sql = "INSERT INTO VALEURS_CAPTEURS (record, tension, courant, pourcentage_BAT, luminosite) VALUES (?,?,?,?,?)"
    with db_ops('sensor') as cursor:
        cursor.execute(
            sql, (created, Tension, Courant, Pourcentage_BAT, Luminosite)
        )


def get_last_sensors_data():
    with db_ops('sensor') as cursor:
        row = cursor.execute(
            "SELECT * FROM VALEURS_CAPTEURS ORDER BY record DESC LIMIT 1"
        ).fetchone()
    return row


def get_oldest_sensors_data():
    with db_ops('sensor') as cursor:
        row = cursor.execute(
            "SELECT * FROM VALEURS_CAPTEURS ORDER BY record ASC LIMIT 1"
        ).fetchone()
    return row


def get_last_sensors_data_as_dict() -> dict:
    # Get table columns:
    keys = get_sensor_list()
    # Get data:
    data = get_last_sensors_data()
    # Make a dictionary:
    result = {}
    if data:
        for n, p in zip(keys, data):
            result[n] = p
        return result
    else:
        return None


def get_sensors_data(n=None):
    if n is None:
        args = {"SELECT * FROM VALEURS_CAPTEURS ORDER BY record DESC"}
    else:
        args = ("SELECT * FROM VALEURS_CAPTEURS ORDER BY record DESC LIMIT ?", (str(n), ))
    with db_ops('sensor') as cursor:
        row = cursor.execute(*args).fetchall()
    return row


def get_sensors_data_as_dict(fields=None, n=None) -> dict:
    # Argument 'fields' par default, ('n' default sera dans  la fonction 'get_sensors_data'):
    if fields is None:
        # Get table columns:
        fields = get_sensor_list()

    # Get data:
    data = get_sensors_data(n=n)

    # Make a dictionary of lists:
    result = {}
    if data:
        for k in fields:
            result[k] = []
        for row in data:
            for k in fields:
                result[k].append(row[k])
        return result
    else:
        return None


def get_oldest_sensors_data_as_dict() -> dict:
    # Get table columns:
    keys = get_sensor_list()
    # Get data:
    data = get_oldest_sensors_data()
    # Make a dictionary:
    result = {}
    if data:
        for n, p in zip(keys, data):
            result[n] = p
        return result
    else:
        return None

