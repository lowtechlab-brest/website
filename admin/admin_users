#!/usr/bin/env python
# -*coding: UTF-8 -*-
#
# This script allows to list users and to modify the 'role' of a registered user.
#
# Created by gmaze on 18/10/2023
__author__ = 'gmaze@ifremer.fr'

import os
import sys
import argparse

APP_ROOT = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))  # Absolute path to Flask website root folder
sys.path.insert(0, APP_ROOT)
import website.db as DB
from website import app


def setup_args():
    icons_help_string = """Script à utiliser pour modifier le 'rôle' d'un utilisateur enregistré"""
    description = """Low-Tech Lab Brest: Gestionnaire des utilisateurs

    Liste des rôles possibles et privilèges associés:
    
    1. Read: Peut lire tout le contenu du site (au cas ou certaines choses seraient reservees aux adherents pas exemple)
    2. Write: Peut écrire un article de blog puis le modifier ou le supprimer
    3. Maintain: Peut modifier ou supprimer n'importe quel article
    4. Triage: Peut modifier le role de n'importe quel utilisateur
    5. Admin: Peut supprimer un utilisateur
    
    Rq: Les privilèges se cumulent
"""

    parser = argparse.ArgumentParser(description=description,
                                     formatter_class=argparse.RawTextHelpFormatter,
                                     epilog="%s\n(c) Low-Tech Lab Brest, 2023" % icons_help_string)

    # Add long and short arguments
    parser.add_argument("-l", "--list", help="Liste des utilisateurs", action='store_true')
    parser.add_argument("--email", help="Email de l'utilisateur à modifier", type=str, default=None)
    parser.add_argument("--set_role", help="Nouveau rôle à appliquer ('read', 'write', 'maintain', 'triage', ou 'admin')", type=str, default=None)

    return parser


def print_header():
    line = "%2s | %26s | %26s | %8s | %20s | %20s | %s" % (
        'id',
        'registration_date',
        'last_connection',
        'role',
        'nom',
        'prenom',
        'email',
    )
    return line

def print_user(user_d):
    line = "%0.2d | %26s | %26s | %8s | %20s | %20s | %s" % (
        user_d['id'],
        user_d['registration_date'],
        user_d['last_connection'],
        user_d['role'],
        user_d['nom'],
        user_d['prenom'],
        user_d['email'],
    )
    return line



if __name__ == "__main__":
    ARGS = setup_args().parse_args()

    with app.app_context():
        if ARGS.list:
            print(print_header())
            users = DB.get_users_as_dict()
            for user in users:
                print(print_user(user))
        elif ARGS.email:
            user = DB.get_user_by_email_as_dict(ARGS.email)
            if 'id' in user:
                sys.stdout.write("Utilisateur trouvé:\n")
                for k in user:
                    sys.stdout.write("%20s: %s\n" % (k, user[k]))
                if ARGS.set_role in ['read', 'write', 'maintain', 'triage', 'admin']:
                    DB.update_user_role(user, ARGS.set_role)
                    if DB.get_user_by_email_as_dict(ARGS.email)['role'] == ARGS.set_role:
                        sys.stdout.write("Rôle mis à jour avec succés '%s'" % ARGS.set_role)
                    else:
                        raise ValueError("Quelque s'est mal passé !")
                elif ARGS.set_role is not None:
                    raise ValueError("Nouveau rôle invalide ('%s'). Les rôles possibles sont: 'read', 'write', 'maintain', 'triage', 'admin'" % ARGS.set_role)

            else:
                raise ValueError("Cet utilisateur ('%s') n'est pas dans la base de données" % ARGS.email)

    # Exit gracefully
    sys.exit(0)
