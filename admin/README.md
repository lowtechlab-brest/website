Dans ce dossier on place les scripts necessaires a la gestion offline de la base de donnees

- init_db: Pour créer les fichiers de DB (vide)
- check_db: Verification des bases de donnees sqlite3 (re-applique les schemas)
- admin_users: CLI pour gerer les roles des utilisateurs


## Liste des rôles des utilisateurs enregistrés

Rq: Les privilèges se cumulent

1. Read: Peut lire tout le contenu du site (au cas ou certaines choses seraient reservees aux adherents pas exemple)
1. Write: Peut écrire un article de blog puis le modifier ou le supprimer
1. Maintain: Peut modifier ou supprimer n'importe quel article
1. Triage: Peut voir la liste des utilisateurs et modifier le role de n'importe lequel
1. Admin: Peut supprimer un utilisateur
