CREATE TABLE IF NOT EXISTS 'VALEURS_CAPTEURS' (
	"id"	INTEGER NOT NULL,
	"record"    TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	"tension"	float,
	"courant"	float,
	"pourcentage_BAT"	float,
	"luminosite"	float,
	PRIMARY KEY("id" AUTOINCREMENT)
);