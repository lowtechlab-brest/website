CREATE TABLE IF NOT EXISTS 'users' (
	"id"	INTEGER NOT NULL,
	"nom"	TEXT,
	"prenom"	TEXT,
	"email"	BLOB,
	"mdp"	TEXT,
	"role"	TEXT,
	"registration_date"	 DATETIME,
	"last_connection"	DATETIME,
	PRIMARY KEY("id" AUTOINCREMENT)
);
