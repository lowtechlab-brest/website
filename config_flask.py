import os
import json

# Turns on debugging features in Flask:
DEBUG = True
TESTING = True

# Définition du fichier dictionnaire 'database.json' crée avec le script 'admin/init_db'.
# Ce dictionnaire contient les chemins absolus vers les bases de données.
# A modifier selon votre configuration:
DB_JSON = '/Users/gmaze/git/github/gmaze/lowtech-lab-website/data/database.json'

# Chargement du dictionnaire :
if not os.path.exists(DB_JSON):
    raise ValueError("""
Impossible d'accéder au fichier dictionnaire des bases de données.
Vérifier que la variable 'DB_JSON' dans le fichier '%s' pointe bien vers un fichier json valide (cad généré avec 'admin/init_db').
Pour l'instant elle pointe vers: '%s' qui n'éxiste pas""" % (__name__, DB_JSON))
else:
    with open(os.path.expanduser(DB_JSON), 'r') as f:
        DATABASE = json.load(f)

# Etat du switch de résolution par défaut des images du blog (au demarrage du site pour un nouvel utilisateur)
LR = False

# Rôle par défaut des nouveaux usagers s'enregistrant sur le site (cf admin/README.md pour les détails)
DEFAULT_ROLE = 'read'

# PARAMETRES D'ENREGISTREMENT DES MESURES CAPTEURS ET DES VARIABLES SERVEUR:
VALEURS_CAPTEURS_REFRESH = 5  # Interval d'enregistrement des mesures capteur en secondes
VALEURS_CAPTEURS_MAXLEN = 86400/5*2  # Nombre maximum d'enregistrements en base, ex: 2 jours a une frequence de 2 secondes
VALEURS_CAPTEURS_MAXAGE = 86400*3  # Age maximum des enregistrements, en secondes
