# Code source du site web du Low-Tech Lab de Brest

La premiere version de ce code est issue du travail d'etudiants [en BTS du lycee de la Croix Rouge a Brest](https://www.lacroixrouge-brest.fr/ecoles-formations/enseignement-superieur/bts-systemes-numeriques/) au printemps 2023.

**🍏 L'idee est d'avoir un site ultra-leger a faible impact environnemental et heberge sur un raspberry-pi alimente par un panneau solaire.**

🫵 Si vous avez des propositions d'ameliorations du site, n'hesitez pas: [🗣 faire une proposition](https://framagit.org/lowtechlab-brest/website/-/issues/new?issuable_template=amelioration_template).

## 🍃 Feuille de route 

1. Notre premier objectif est de mettre en ligne une version utilisable et diffusable du site web le plus rapidement possible. La progression vers cette premiere etape peut etre [suivie ici](https://framagit.org/lowtechlab-brest/website/-/milestones/1#tab-issues)
2. La seconde etape sera de mettre en ligne le site web sur un raspberry alimente par un panneau solaire. [Suivi ici](https://framagit.org/lowtechlab-brest/website/-/milestones/2#tab-issues)

## 🗂 Structure du repository

- ``data``: repertoire de stockage de la base de données (non versionné sur framagit pour des raisons de sécurité)
- ``website``: code du site web implementé comme une [application Flask](https://flask.palletsprojects.com/)
- ``admin``: scripts utiles a la gestion de la base de donnees

## 📸 Gestion des images des posts du blog

- L'image importée est supposée de haute qualité (qualité d'origine).
- L'image est dupliquée et modifiée pour en créer une image qui charge rapidement.
- L'image possède une version dégradée quand le site aurait une faible batterie.

La gestion des images est faite par le script [website/degradation_image.py](https://framagit.org/lowtechlab-brest/website/-/blob/19f3d0e15be9f2fd78813bc4a487cac777f6abb7/website/image.py) 

## 🔗 Liens utiles:
- le site web "officiel" est ici: https://www.lowtechlabbrest.org/
- pour adherer: https://www.helloasso.com/associations/low-tech-lab-brest/adhesions/adhesion-low-tech-lab-brest/
- code couler du LTLB: https://coolors.co/d5b0ac-cea0ae-684551-402e2a-9cd08f
- email de contact: bonjour @ lowtechlabbrest.org

- sur les reseaux:
  - https://www.linkedin.com/company/low-tech-lab-brest/
  - https://www.facebook.com/lowtechlabbrest
  - https://discord.gg/2MS25eYT

## 🤿 Comment ça marche ?

- Le site web est une application [Flask](https://flask.palletsprojects.com).
- Apres avoir recupere le code de ce depot (et installer l'environnement python approprie), on lance la commande ``flask --app website run --debug`` pour tester le site en local


### 📏 Qualité variable des images

- Pour minimiser les transferts de donnees et l'usage de la batterie, il y a un bouton sur le site qui permet de modifier la qualite des images
- Cette fonctionalite s'appuie en partie sur le script ``images.py``, des qu'un nouveau fichier image est uploade, 2 versions haute et basse resolution de l'image sont crees.

### 🗜Installation du site web sur un Raspberry

#### Install de l'environnement de base pour travailler

- [ ] Configurer le Raspberry pour pouvoir s'y connecter en SSH
- [ ] Se connecter au Raspberry en ssh
- [ ] Installer micromamba:
```bash
  "${SHELL}" <(curl -L micro.mamba.pm/install.sh)
```  
- [ ] Relancer une connection au Raspberry pour sourcer le bash mis a jour (avec la config micromamba)
- [ ] Installer git dans l'environnement conda ``base``:
```bash
micromamba activate base
micromamba install git
```
- [ ] Créer un dossier pour accueillir le code du site web:
```bash
mkdir framagit
mkdir framagit/lowtech-lab-brest
```
- [ ] Clone le repo du site web dans ce dossier:
```bash
cd framagit/lowtech-lab-brest
git clone https://framagit.org/lowtechlab-brest/website.git
```
- [ ] Créer l'environnement Python de travail:
```bash
cd framagit/lowtech-lab-brest/website
micromamba env create -f environment.yml
```
- [ ] Activer l'environnement Python ``lowtechlab``:
```bash
micromamba activate lowtechlab
```

#### Initialisation du site web

- [ ] Créer les bases de données:
Les fichiers de base de donnees peuvent etre stockes n'importe ou, sauf dans le dossier ou se situe l'application flask ("siteweb"). Ici, par exemple, on va les placer dans un repertoire "data" au niveau du repository:
```bash
cd framagit/lowtech-lab-brest/website
admin/init_db data  # Creer les bases de donnees dans le repertoire "data"
```
- [ ] Avec les fichiers de bases de données, le script ci-dessus a crée un petit fichier dictionnaire `database.json`. Il faut donc s'assurer que le fichier de configuration Flask ``config_flask.py`` pointent bien vers ce dictionnaire. Pour cela ouvrir ``config_flask.py`` et modifier la variable ``DB_JSON`` avec le chemin vers le fichier dictionnaire.
Par default, elle pointe vers ``'data/database.json'`` pour suivre l'exemple ci-dessus.
- [ ] Verifier la connection vers les bases:
```bash
cd framagit/lowtech-lab-brest/website
python admin/check_db
```
Si vous avez une erreur à cette étape, c'est probablement que la précédante n'a pas été bien faite, vérifier la variable ``DB_JSON`` dans le fichier ``config_flask.py``.

#### Test du site

Verifier que toutes les urls sont disponibles:
```bash
cd framagit/lowtech-lab-brest/website
flask --app website routes
```

puis lancer le site pour les tester:
```bash
cd framagit/lowtech-lab-brest/website
flask --app website run --debug -p 5000
```